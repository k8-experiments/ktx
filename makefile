build:
	- mkdir bin
	go build -o ./bin/ktx -ldflags="-s -w" ./...
	upx ./bin/ktx

run:
	go run ./...


housekeeping:
	go get ./...
	go fmt ./...

clean:
	- rm -r ./bin

install:
	sudo cp ./bin/ktx /usr/bin/ktx
