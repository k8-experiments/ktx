package main

import (
	"bufio"
	"fmt"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
)

const (
	red   = "\033[31m"
	reset = "\033[0m"
	green = "\033[32m"
)

type kConfig struct {
	CurrentContext string `yaml:"current-context"`
}

func main() {
	h, err := os.UserHomeDir()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	d, err := ioutil.ReadFile(h + "/.kube/config")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	var kc kConfig

	err = yaml.Unmarshal(d, &kc)

	reader := bufio.NewReader(os.Stdin)

	var text string
	for {
		fmt.Print("Use ", red+kc.CurrentContext+reset, "? ")

		text, err = reader.ReadString('\n')
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		switch text {
		case "yes\n":
			fmt.Println("Using", green+kc.CurrentContext+reset)
			os.Exit(0)
		case "no\n", "n\n":
			os.Exit(1)
		default:
			fmt.Println("yes or no")
		}
	}
}
