# ktx

ktx is a small utility that stops me from running kubernetes related commands on the wrong cluster by asking if this is the correct cluster/context.

![ktx](./doc/ktx.gif)

## Build an install

Ensure that the following is installed:
 - go 1.16 +
 - make
 - upx

### 1. Build

```bash
make build
```

### 2. Install

```bash
make install
```

### 3. Alias

```bash
alias kubectl="ktx && kubectl"
```
